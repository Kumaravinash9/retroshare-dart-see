import 'dart:async';
import 'dart:convert';
import 'dart:io';

import "package:eventsource/eventsource.dart";

main() async {
  var body = {
    'eventType': 15
  };
  print("Subscribing..");
  String url = "http://127.0.0.1:9092/rsEvents/registerEventsHandler";
  String auth = "user:pass";
  EventSource eventSource =
  await EventSource.connect(url,
    method: "POST",
    body: body,
    headers: {
      HttpHeaders.authorizationHeader:
      'Basic ' + base64.encode(utf8.encode('auth'))
    },
  );

  // Used to store it and close connections
  StreamSubscription<Event> streamSubscription = eventSource.listen((Event event) {
    print("New event:");
    print("  event: ${event.event}");
    print("  data: ${event.data}");
  });
}
